import sys
import bpy

from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       EnumProperty,
                       PointerProperty,
                       )
                       
sys.path.append(r'C:\Users\jonat\Git Repos/blender-tools/Python')

#from Rename import Renamer
#from CreateControls import MasterControlCreator
#from BoundingBox import LocatorTool
#from JointBuilder import JointCreator
#from RandomPlacement import RandomPlacement

#MCC = MasterControlCreator()
#LocTool = LocatorTool()
#JointCreator = JointCreator()
#RandomPlacement = RandomPlacement()

#class View3DPanel():

# ------------------------------------------------------------------------
#    properties
# ------------------------------------------------------------------------

class MySettings(bpy.types.PropertyGroup):
    
    bpy.types.Scene.toRename = StringProperty(
        name="User Input",
        description=":",
        default="Prefix_#_Suffix",
        maxlen=1024,
        )
        
    bpy.types.Scene.numPadding = IntProperty(
        name = "Num-Padding",
        description="How much num-padding",
        default = 2,
        min = 1,
        max = 100
        )
        
    bpy.types.Scene.duplicates = IntProperty(
        name = "Duplicates",
        description="Number of times to duplicate",
        default = 2,
        min = 1,
        max = 100
        )
        
    bpy.types.Scene.minRange = FloatProperty(
        name = "Min Range",
        description = "Lower bound of scattering box",
        default = -10,
        min = -10000.0,
        max = 10000.0
        )
        
    bpy.types.Scene.maxRange = FloatProperty(
        name = "Max Range",
        description = "Upper bound of scattering box",
        default = 10,
        min = -10000.0,
        max = 10000.0
        )
        
    bpy.types.Scene.customColor = bpy.props.FloatVectorProperty(
        name="Control Color",
        description="Color value",
        default = (0.0, 0.0, 0.0),
        subtype='COLOR'
        )
        
bpy.utils.register_class(MySettings)


# ------------------------------------------------------------------------
#    operators
# ------------------------------------------------------------------------

class RenamerOperator(bpy.types.Operator):
    bl_idname = "wm.renamer_operator"
    bl_label = "Rename"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")


class DuplicatorOperator(bpy.types.Operator):
    bl_idname = "wm.duplicator_operator"
    bl_label = "Duplicate"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class CenterPivotOperator(bpy.types.Operator):
    bl_idname = "wm.centerpivot_operator"
    bl_label = "Center (Pivot)"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class CenterBoxOperator(bpy.types.Operator):
    bl_idname = "wm.centerbox_operator"
    bl_label = "Center (Bounding Box)"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class JointCreatorOperator(bpy.types.Operator):
    bl_idname = "wm.jointcreator_operator"
    bl_label = "Create FK Chain"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class CircleControlOperator(bpy.types.Operator):
    bl_idname = "object.circlecontrol_operator"
    bl_label = "Circle"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class BoxControlOperator(bpy.types.Operator):
    bl_idname = "object.boxcontrol_operator"
    bl_label = "Box"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class StarControlOperator(bpy.types.Operator):
    bl_idname = "object.starcontrol_operator"
    bl_label = "Star"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
        
class ControlCreatorOperator(bpy.types.Operator):
    bl_idname = "wm.controlcreator_operator"
    bl_label = "Create Controls"

    
    def execute(self, context):
        # print the values to the console
        print("Hello World")
        
bpy.utils.register_class(RenamerOperator)
bpy.utils.register_class(DuplicatorOperator)
bpy.utils.register_class(CenterPivotOperator)
bpy.utils.register_class(CenterBoxOperator)
bpy.utils.register_class(JointCreatorOperator)
bpy.utils.register_class(CircleControlOperator)
bpy.utils.register_class(BoxControlOperator)
bpy.utils.register_class(StarControlOperator)
bpy.utils.register_class(ControlCreatorOperator)


# ------------------------------------------------------------------------
#    menus
# ------------------------------------------------------------------------

class ControlShapeMenu(bpy.types.Menu):
    bl_idname = "OBJECT_MT_controlshapemenu"
    bl_label = "Control Shape:"

    def draw(self, context):
        layout = self.layout

        # built-in example operators
        layout.operator("object.circlecontrol_operator", text="Circle")
        layout.operator("object.boxcontrol_operator", text="Box")
        layout.operator("object.starcontrol_operator", text="Star")

bpy.utils.register_class(ControlShapeMenu)

# ------------------------------------------------------------------------
#    panels
# ------------------------------------------------------------------------


class Renamer(bpy.types.Panel):
    bl_label = "Renamer"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Master Control Program"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        
        # COLUMN LAYOUT
        col = layout.column()
        
        col.prop(scene, "toRename")
        col.prop(scene, "numPadding")
        col.operator("wm.renamer_operator", icon="GREASEPENCIL")
        
        
class RandomPlacement(bpy.types.Panel):
    bl_label = "Random Placement"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Master Control Program"
    
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        
        col = layout.column()
        col.prop(scene, "duplicates")
        col.prop(scene, "minRange")
        col.prop(scene, "maxRange")
        col.operator("wm.duplicator_operator", icon="PIVOT_INDIVIDUAL")
        
        
class CenterLocator(bpy.types.Panel):
    bl_label = "Center Locator"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Master Control Program"
        
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        
        col = layout.column()
        col.operator("wm.centerpivot_operator")
        col.operator("wm.centerbox_operator")
        
        
class JointCreator(bpy.types.Panel):
    bl_label = "Joint Creator"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Master Control Program"
        
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        
        col = layout.column()
        col.operator("wm.jointcreator_operator", icon="BONE_DATA")
        
        
class ControlCreator(bpy.types.Panel):
    bl_label = "Control Creator"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Master Control Program"
      
    def draw(self, context):
        layout = self.layout
        scene = context.scene

        col = layout.column()
        col.menu("OBJECT_MT_controlshapemenu", text="SELECT SHAPE", icon="CURVE_BEZCIRCLE")
        col.prop(scene, "customColor")
        col.operator("wm.controlcreator_operator", icon="OUTLINER")
        
        
#classes = (
#    MySettings,
#    Renamer
#)

#register, unregister = bpy.utils.register_classes_factory(classes)

def register():
    bpy.utils.register_class(Renamer)
    bpy.utils.register_class(RandomPlacement)
    bpy.utils.register_class(CenterLocator)
    bpy.utils.register_class(JointCreator)
    bpy.utils.register_class(ControlCreator)
    
def unregister():
    bpy.utils.unregister_class(MySettings)
    bpy.utils.unregister_class(Renamer)
    bpy.utils.unregister_class(CenterLocator)
    bpy.utils.unregister_class(JointCreator)
    bpy.utils.unregister_class(ControlCreator)
    
if __name__ == "__main__":
    register()

print("MCP Loaded")